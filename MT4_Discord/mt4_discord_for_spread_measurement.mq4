#property copyright ""
#property link      ""
#property version   "1.00"
#property strict

#define OPEN_TYPE_PRECONFIG                0           // use the configuration by default
#define FLAG_KEEP_CONNECTION               0x00400000  // do not terminate the connection
#define FLAG_PRAGMA_NOCACHE                0x00000100  // no cashing of the page
#define FLAG_RELOAD                        0x80000000  // receive the page from the server when accessing it
#define SERVICE_HTTP                       3           // the required protocol
#define DEFAULT_HTTPS_PORT                 443
#define FLAG_SECURE                        0x00800000  // use PCT/SSL if applicable (HTTP)
#define INTERNET_FLAG_SECURE               0x00800000
#define INTERNET_FLAG_KEEP_CONNECTION      0x00400000
#define HTTP_ADDREQ_FLAG_REPLACE           0x80000000
#define HTTP_ADDREQ_FLAG_ADD               0x20000000


#include <stderror.mqh>
#include <stdlib.mqh>


#import "wininet.dll"
int InternetOpenW(string sAgent,int lAccessType,string sProxyName,string sProxyBypass,int lFlags);
int InternetConnectW(int hInternet,string ServerName, int nServerPort,string lpszUsername, string lpszPassword, int dwService,int dwFlags,int dwContext);
int HttpOpenRequestW(int hConnect, string Verb, string ObjectName, string Version, string Referer, string AcceptTypes, int dwFlags, int dwContext);
int HttpSendRequestW(int hRequest, string &lpszHeaders, int dwHeadersLength, uchar &lpOptional[], int dwOptionalLength);
int InternetReadFile(int hFile,uchar &sBuffer[],int lNumBytesToRead,int &lNumberOfBytesRead);
int InternetCloseHandle(int hInet);
#import

input string url = "https://discord.com/api/webhooks/123123123/xxxxxxxxxxx"; // DiscordのWebhook URL


int OnInit()
{
  if (MQLInfoInteger(MQL_DLLS_ALLOWED) != 1) {
    Print("==================");
    Print("DLL is not allowed");
    Print("==================");
    return(INIT_FAILED);
  }

  int check = GetLastError(); // エラーコード取得
  Print("check : ", check);

  if( check != ERR_NO_ERROR) {
    Print("エラーコード: ",check ," 詳細：",ErrorDescription(check));
  }

  string webhookPath;

  //列文字列内の「/api/webhooks/」の最初の出現位置を返す
  int startPos = StringFind(url, "/api/webhooks/");

  //出現位置が-1ではない場合文字列を抽出する
  if(startPos != -1) {
    webhookPath = StringSubstr(url, startPos);
  }else{
    Print("Error: /api/webhooks/ not found in the given URL!");
  }

  SendNotificationToDiscord(webhookPath, "Hello");
  return(INIT_SUCCEEDED);
}


void OnDeinit(const int reason)
{
}


void OnTick()
{
}


void SendNotificationToDiscord(string objectname, string message) {
  string headers = "Content-Type: application/json\r\n";
  string dataString = "{\"username\": \"MT4スプレッド計測用_監視EA\", \"content\":\"" + message + " \"}";

  uchar data[];
  StringToCharArray(dataString, data, 0, -1, CP_UTF8);

  int Session = InternetOpenW("MetaTrader 4 Terminal", 0, "", "", 0);
  int connect = InternetConnectW(Session, "discord.com", DEFAULT_HTTPS_PORT, "", "", SERVICE_HTTP, 0, 0);
  int hRequest = HttpOpenRequestW(connect, "POST", objectname, "HTTP/1.1", NULL, NULL, (int)(FLAG_SECURE | FLAG_KEEP_CONNECTION | FLAG_RELOAD | FLAG_PRAGMA_NOCACHE), 0);
  int hSend = HttpSendRequestW(hRequest, headers, StringLen(headers), data, ArraySize(data) - 1);

  if (hSend <= 0) {
    Print("Error Sending Notification");
  } else {
    Print("Notification Sent Successfully!");
  }

  InternetCloseHandle(hSend);
  InternetCloseHandle(hRequest);
  if (Session > 0) InternetCloseHandle(Session);
  if (connect > 0) InternetCloseHandle(connect);
}
